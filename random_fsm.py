#!/bin/python

import lib
import random

ranges = [
  (1, 4),  # min and max # of inputs
  (1, 4),  # min and max # of inputs
  (2, 20)  # min and max # of states
]

dimensions = tuple(random.randint(*r) for r in ranges)
table = lib.random_table(tuple(dimensions))
dot = lib.dot(table)
print(dot)
