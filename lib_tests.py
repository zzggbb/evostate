import lib
from test_manager import TestManager

tests = TestManager()

tests.test("stringify", lib.stringify([0, 1, 0, 1]), "0101")

t1 = [
  [[0, 2, 3, 1], [0, 1, 0]],
  [[3, 3, 2, 0], [1, 1, 0]],
  [[3, 0, 1, 1], [0, 0, 0]],
  [[2, 1, 0, 0], [0, 1, 0]]
]

tts1 = "00101101010111110001101100010100010010000010"

dimensions1 = (2, 3, 4)

tests.test("encode table -> string", lib.encode(t1), tts1)
tests.test("decode tts -> table", lib.decode(tts1, dimensions1), t1)
tests.test("tts length", lib.tts_length(dimensions1), len(tts1))
tests.test_in("random tts", lib.random_tts((1,1,1)), ["000", "001"])

unreachable = [
  [[0, 2], []],
  [[0, 3], []],
  [[3, 1], []],
  [[1, 2], []],
  [[4, 3], []]
]

tests.test("unreachable states",
           lib.unreachable(unreachable),
           {4}
)

duplicates = [
  [[], [0, 1, 0]],
  [[], [1, 1, 1]],
  [[], [0, 0, 1]],
  [[], [1, 1, 1]],
  [[], [0, 1, 0]],
]

tests.test("output groups", lib.indistinguishable(duplicates),
    {'010': [0, 4], '111': [1, 3], '001': [2]}
)

tests.report()
