class TestManager(object):
  def __init__(self):
    self.fail_count = 0
    self.total_count = 0
    self.fail_info = []
    self.perfect_message = "all tests passed"

  def test_in(self, name, actual, options):
    # test that actual is an element in options
    success = actual in options
    status = "pass" if success else "fail"

    self.total_count += 1
    if not success:
      self.fail_count += 1
      self.fail_info.append((name, actual, options))

  def test(self, name, actual, expected):
    success = actual == expected
    status = "pass" if success else "fail"

    self.total_count += 1
    if not success:
      self.fail_count += 1
      self.fail_info.append((name, actual, expected))


  def report(self):
    if not self.fail_count:
      print(self.perfect_message)

    else:
      template = "{} of {} tests failed"

      print(template.format(self.fail_count, self.total_count))

      for (name, actual, expected) in self.fail_info:
        template = "{}: expected: {}\n{}actual: {}"
        print(template.format(name, expected, ' '*(len(name) + 4), actual))

