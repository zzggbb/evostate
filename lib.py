#!/usr/bin/python

import math
import random

def encoding_bits(n):
  """
  Calculate the # of bits needed to encode integer `n`. eg:

    encoding_bits(1) -> 1
    encoding_bits(2) -> 1
    encoding_bits(3) -> 2
    encoding_bits(8) -> 3

  """
  return 1 if n == 1 else math.ceil(math.log2(n))

def chunks(seq, size):
  """
  Generate sequential chunks from `seq` of size `size`. eg:

    chunks [1 2 3 4 5 6] 2 -> [[1 2] [3 4] [5 6]]

  Even divisibility is not necessary.
  Therefore elements returned are not guaranteed to be of size `size`. eg:

    chunks [1 2 3 4 5 6] 4 -> [[1 2 3 4] [5 6]]

  """
  for i in range(0, len(seq), size):
    yield seq[i:i+size]

def stringify(seq):
  return ''.join(map(str, seq))

def tts_length(dimensions):
  """
  Calculate the # of bits needed to encode a TTS form FSM with shape
  `dimensions`.
  Dimensions is a tuple with form (# of inputs, # of outputs, # of states).
  eg:

    tts_length (1 1 1) -> 3
    tts_length (2 3 4) -> 44

  """
  i, o, s = dimensions
  t_bits = 2**(i) * s * encoding_bits(s)
  o_bits = s*o
  return t_bits + o_bits

def random_table(dimensions):
  """
  Generate a random ALT format FSM with shape `dimensions`.
  Dimensions is a tuple of format (# of inputs, # of outputs, # of states).
  eg:

    random_table (1 1 1) -> [[[0 0] [1]]]
    random_table (2 1 3) -> [[[0 0 1 0] [0]] [[1 1 0 0] [0]] [[2 2 2 1] [1]]]

  """
  i, o, s = dimensions
  table = []
  for _ in range(s):
    transitions = [random.randrange(0, s) for _ in range(2**i)]
    outputs = [random.randint(0,1) for _ in range(o)]
    table.append([transitions, outputs])
  return table

def random_tts(dimensions):
  """
  Generate a random TTS formatted FSM with shape `dimensions`.
  Dimensions is a tuple with form (# of inputs, # of outputs, # of states).
  eg:

    random_tts (2 1 3) -> '101001100100100011010000100'
    random_tts (2 2 4) -> '0000000111110001001111101011110000011110'

  """
  return encode(random_table(dimensions))

def encode(table):
  """
  Convert an ALT form FSM `table` to string form.
  eg:

    encode [[[0, 1], [0]], [[1, 0], [0]], [[2, 2], [1]]] -> '000100100010101'

  """
  n_states = len(table)
  bps = encoding_bits(n_states)
  tts = ''
  bit_fmt = '0' + str(bps) + 'b'
  for row in table:
    transitions, outputs = row

    for transition in transitions:
      tts += format(transition, bit_fmt)

    for output in outputs:
      tts += str(output)

  return tts

def decode(tts, dimensions):
  """
  Convert a TTS form FSM `tts` to ALT form.
  Dimensions is a tuple with form (# of inputs, # of outputs, # of states).
  eg:

    decode '000100100010101' -> [[[0, 1], [0]], [[1, 0], [0]], [[2, 2], [1]]]

  """
  n_inputs, n_outputs, n_states = dimensions
  table = []
  bps = encoding_bits(n_states)
  transition_length = bps * n_states
  row_length = transition_length + n_outputs

  for i in range(n_states):
    start = row_length * i
    end_transitions = start + transition_length

    t_slice = slice(start, end_transitions)
    o_slice = slice(end_transitions, end_transitions+n_outputs)

    transitions = [int(b, 2) for b in chunks(tts[t_slice], bps)]
    outputs = [int(b, 2) for b in chunks(tts[o_slice], 1)]

    table.append([transitions, outputs])

  return table

def dot(table, input_label="i=", output_label="o="):
  """
  Generate a DOT graph representation of the ALT form FSM `table`.
  eg:

    dot [[[2, 2], [0]], [[2, 0], [0]], [[0, 1], [0]]] ->

    # inputs=1 outputs=1 states=3
    digraph fsm {
      node [shape=record]
      0 [label="0|o=0"]
      0 -> 2 [label="i=0"]
      0 -> 2 [label="i=1"]
      1 [label="1|o=0"]
      1 -> 2 [label="i=0"]
      1 -> 0 [label="i=1"]
      2 [label="2|o=0"]
      2 -> 0 [label="i=0"]
      2 -> 1 [label="i=1"]
    }

  """
  n_inputs = encoding_bits(len(table[0][0]))
  n_outputs = len(table[0][1])
  n_states = len(table)

  out = "# inputs={} outputs={} states={}\n".format(n_inputs, n_outputs, n_states)
  out += "digraph fsm {\n"
  out += "node [shape=record]\n"

  bit_fmt = '0' + str(n_inputs) + 'b'

  for i,row in enumerate(table):
    transitions, outputs = row

    state_template = '{i} [label="{i}|{label}{output}"]\n'
    out += state_template.format(i=i, label=output_label, output=stringify(outputs))

    for j,t in enumerate(transitions):
      input_vector = format(j, bit_fmt)
      transition_template = '{src} -> {dest} [label="{label}{vector}"]\n'
      out += transition_template.format(src=i, dest=t, label=input_label,
                                        vector=input_vector)

  out += '}'
  return out

def unreachable(table):
  """
  Determine the unreachable states in ALT formatted FSM `table`.
  """

  reachable = {0}
  new = {0}

  while new:

    # find all destinations available from sources in new
    reachable_from_new = set()
    for source in new:
      reachable_from_new |= set(table[source][0])

    # otherwise the search would never end
    new = reachable_from_new - reachable

    reachable |= new

  return set(range(len(table))) - reachable

def indistinguishable(table):
  """
  Determine the indistinguishable states in ALT formatted FSM `table`.
  Returns a list of groupings of distinguishable states. eg:

    An FSM has 4 states: A B C D. If C and D are indistinguishable, while
    A and B distinguishable, then:

      indistinguishable FSM -> [[A] [B] [C D]]

  """

  output_groups = {}
  for i, row in enumerate(table):
    output = stringify(row[1])
    if output in output_groups:
      output_groups[output].append(i)
    else:
      output_groups[output] = [i]

  return output_groups


def minimize(table):
  """
  Minimize the ALT form FSM `table`.
  Unreachable states are removed, then undistinguishable states are merged.
  """
  pass
